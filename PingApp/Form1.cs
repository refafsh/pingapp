﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net.NetworkInformation;
using System.IO;
using System.Threading;

namespace PingApp
{
    public partial class Form1 : Form
    {
        bool run = false;
        string filename = @"C:\temp\pingresult.txt";
        string res = "";

        public Form1()
        {
            InitializeComponent();
            lblFilename.Text = filename;
         }

        private void btnStart_Click(object sender, EventArgs e)
        {

            if (textBox1.Text != "")
            {
                ToggleButtons();
                run = true;

                Thread traad = new Thread(pingdevice);
                traad.Start();
            }
            else
            {
                MessageBox.Show("You have to put in a host!");
            }
        }

        private void pingdevice()
        {
            while (run)
            {
                try
                {
                    Ping p = new Ping();
                    PingReply r;
                    string s;
                    s = textBox1.Text;
                    r = p.Send(s);

                    if (r.Status == IPStatus.Success)
                    {
                        res = "Ping to " + s.ToString() + "[" + r.Address.ToString() + "]" + " Successful"
                           + " Response delay = " + r.RoundtripTime.ToString() + " ms" + "\n";
                    }
                    else
                    {
                        res = "fejl";
                    }
                    res = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + res;

                    File.AppendAllText(filename, res);
                } catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    run = false;
                }
                System.Threading.Thread.Sleep(5000);
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            run = false;
            ToggleButtons();
        }

        private void ToggleButtons()
        {
            btnStart.Enabled = !btnStart.Enabled;
            btnStop.Enabled = !btnStop.Enabled;
            btnSetFilename.Enabled = !btnStart.Enabled; ;
        }

        private void SetFileName()
        {
            SaveFileDialog fsd = new SaveFileDialog();
            fsd.ShowDialog();
            filename = fsd.FileName;
            lblFilename.Text = filename;

        }

        private void btnSetFilename_Click(object sender, EventArgs e)
        {
            SetFileName();
        }
    }
}
